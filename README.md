# Лабораторна робота №5. Циклічні конструкції
# 1 ВИМОГИ
## 1.1 Розробник
Бондар Артем 
КIТ - 121 Б
## 1.2 Загальне завдання
Індивідуальні завдання.
## 1.3 Задача
```
23. (**) Визначити, чи є задане ціле число простим.
```
# 2 ОПИС ПРОГРАМИ
## 2.1 Функціональне призначення
Визначити, чи є задане ціле число простим.
## 2.2 Опис логічної структури
Ми перiвiремо число на те що воно бiльше 1 бо прости числа починаються з цифри "2", ми перевiряэмо усi числа до нашого щоб вони не дiлилось на наше числа.
## 2.3 Важливі фрагменти програми
```
// функция проверяет - простое ли число n
int isPrime_For(int n)
{
    // если n > 1
    if (n > 1)
    {
        // в цикле перебираем числа от 2 до n - 1
        for (int i = 2; i < n; i++){
            if (n % i == 0) // если n делится без остатка на i - возвращаем 0 (число не простое)
            {
                return 0;
            }
        }
        // если программа дошла до данного оператора, то возвращаем 1 (число простое) - проверка пройдена
        return 1;
    }
    else // иначе возвращаем 0 (число не простое)
        return 0;
}
////////////////
int isPrime_While(int n)
{
    // если n > 1
    if (n > 1)
    {
        int i = 2;
        // в цикле перебираем числа от 2 до n - 1
        while(i < n){
            i++;
            if (n % i == 0) // если n делится без остатка на i - возвращаем 0 (число не простое)
            {
                return 0;
            }
        }
        // если программа дошла до данного оператора, то возвращаем 1 (число простое) - проверка пройдена
        return 1;
    }
    else // иначе возвращаем 0 (число не простое)
        return 0;
}
////////////////
int isPrime_Do_While(int n)
{
    // если n > 1
    if (n > 1)
    {
        // в цикле перебираем числа от 2 до n - 1
        int i = 2;
        do{
            i++;
            if (n % i == 0) // если n делится без остатка на i - возвращаем 0 (число не простое)
            {
                return 0;
            }
        }while(i < n-1);
        // если программа дошла до данного оператора, то возвращаем 1 (число простое) - проверка пройдена
        return 1;
    }
    else // иначе возвращаем 0 (число не простое)
        return 0;
}
```
# 3 ВАРІАНТИ ВИКОРИСТАННЯ
# ВИСНОВКИ
